package main;

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import javax.swing.border.EmptyBorder;

public class HelpAndAbout extends JFrame {

    private int bound = 10;

    public HelpAndAbout(int type, String title) {
        add(createContent(type));
        setTitle(title);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
    }

    private JPanel createContent(int type) {
        JTextArea ta = new JTextArea();
        ta.setWrapStyleWord(true);
        ta.setLineWrap(true);
        ta.setBackground(null);
        ta.setEditable(false);
        ta.setColumns(50);
        ta.setRows(15);
        String text;
        if (type == 0) {
            text = "Bài tập lớn Lập trình hướng đối tượng nhóm 10\n"
                    + "- File:\n"
                    + "\t+ New: Tạo bảng mới hoàn toàn\n"
                    + "\t+ Open: Mở và đọc file .txt\n"
                    + "\t+ Save PNG: Lưu đồ thị đã vẽ thành dạng .png\n"
                    + "\t+ Save TXT: Lưu đồ thị đã vẽ thành dạng .txt\n"
                    + "\t+ Exit: Thoát chương trình\n"
                    + "- Help:\n"
                    + "\t+ Help: Hướng dẫn sử dụng\n"
                    + "\t+ About: Thông tin người tạo\n"
                    + "- Map Type: Chọn đồ thị có hướng hay không\n"
                    + "- Input Random: Đọc input có sẵn\n"
                    + "- Point: Chọn đỉnh bắt đầu và kết thúc để thực hiện thuật toán\n"
                    + "- Run: Hiển thị tất cả đường đi từ đỉnh đầu đến đỉnh cuối\n"
                    + "\t+ Sau khi có đường đi giữa 2 đỉnh, chọn 1 đường bất kì để in ra màn hình\n"
                    + "\t+ Chọn đường đi trước hoặc sau\n"
                    + "- Algorithm: Chọn thuật toán để chạy chương trình\n"
                    + "\t+ BFS: Thể hiện lần lượt các bước chạy BFS từ đỉnh đầu\n"
                    + "\t+ Dijkstra: Hiển thị đường đi ngắn nhất từ đỉnh đầu đến đỉnh cuối\n"
                    + "- Log: Thể hiện quá trình thực hiện chương trình\n"
                    + "- Các nút thực hiện nhanh:\n"
                    + "\t+ New\n"
                    + "\t+ Open\n"
                    + "\t+ SavePNG\n"
                    + "\t+ SaveTXT\n"
                    + "\t+ Draw Point: Thêm đỉnh mới vào chỗ mình click vào\n"
                    + "\t+ Draw Line: Thêm cạnh mới giữa 2 đỉnh được chọn\n"
                    + "\t+ Move: Di chuyển các đỉnh tùy ý\n"
                    + "\t+ Update: Cập nhật lại đồ thị\n"
                    + "- Thao tác khi ấn chuột phải:\n"
                    + "\t+ Change cost: Thay đổi khoảng cách giữa 2 đỉnh\n"
                    + "\t+ Delete: Xóa cạnh hoặc đỉnh được chọn";
        } else {
            text = "Thành viên nhóm:\n"
                    + "1. Đoàn Viết Thắng - 20194165\n"
                    + "2. Đào Nguyễn Tiến Huy -  20194077\n"
                    + "3. Trương Quang Thịnh - 20194179\n"
                    + "4. Nguyễn Lê Tài - 20194162\n"
                    + "5. Nguyễn Văn Thịnh - 20194178\n"
                    + "6. Trương Quang Phú - 20194138\n"
                    + "Giáo viên hướng dẫn: Trần Nhật Hóa";
        }
        ta.append(text);
        JScrollPane scrollPanel = new JScrollPane(ta);
        scrollPanel.setBorder(BorderFactory
                .createEtchedBorder(EtchedBorder.LOWERED));
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(scrollPanel, BorderLayout.CENTER);
        panel.setBorder(new EmptyBorder(bound, bound, bound, bound / 2));
        return panel;
    }
}
