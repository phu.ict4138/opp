package main;

import java.awt.Color;
import java.awt.Font;

public class Config {
	public static Font font = new Font("Cambria", 1, 15);
	public static int R = 15;
	public static int D = 2*R;
	public static double phi = Math.PI / 6;
	public static int barb = 10;
	public static Color colorBackGround = Color.WHITE;
	public static Color colorCost = Color.BLACK;
	public static Color colorIndex = Color.BLACK;
	public static Color colorDraw = Color.WHITE;
	public static Color colorResult = Color.MAGENTA;
	public static Color colorBegin = Color.GREEN;
	public static Color colorEnd = Color.RED;
	public static Color colorLine = Color.BLACK;
	
}
