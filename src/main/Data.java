package main;

import java.io.Serializable;
import java.util.ArrayList;

public class Data implements Serializable {
	private ArrayList<Points> pointList;
	private ArrayList<Lines> lineList;

	public Data() {
		this.pointList = new ArrayList<Points>();
		this.lineList = new ArrayList<Lines>();
	}

	public ArrayList<Points> getArrPoints() {
		return this.pointList;
	}

	public void setArrPoints(ArrayList<Points> pointList) {
		this.pointList = pointList;
	}

	public ArrayList<Lines> getArrLines() {
		return this.lineList;
	}

	public void setArrLines(ArrayList<Lines> lineList) {
		this.lineList = lineList;
	}

}
