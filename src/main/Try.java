package main;

import java.util.ArrayList;

public class Try {
    private int size;
    private int line;
    private int beginPoint, endPoint;
    private int a[][];
    private ArrayList<int[]> result = new ArrayList<int[]>();
    private ArrayList<Integer> space = new ArrayList<Integer>();
    private int[] trace;
    private boolean[] visited;
    private ArrayList<Points> arrPoints = new ArrayList<Points>();
    private ArrayList<Lines> arrLines = new ArrayList<Lines>();
    private int cost;
    private boolean mapType = false;

    private int count;

    public Try() {
    }

    public void input() {
        count = 0;
        result.clear();
        size = arrPoints.size();
        a = new int[size][size];
        trace = new int[size];
        visited = new boolean[size];
        size--;
        for (int i = 1; i <= size; i++) {
            visited[i] = false;
        }

        trace[0] = beginPoint;
        visited[beginPoint] = true;

        line = arrLines.size();
        for (int i = 1; i < line; i++) {
            a[arrLines.get(i).getIndexPointA()][arrLines.get(i).getIndexPointB()] = arrLines.get(i).getCost();
            if (!mapType) {
                a[arrLines.get(i).getIndexPointB()][arrLines.get(i).getIndexPointA()] = arrLines.get(i).getCost();
            }
        }
    }

    public void Sol(int line) {
        count++;
        int arr[] = new int[line];
        cost = 0;
        for (int i = 0; i < line; i++) {
            arr[i] = trace[i];
            if (i > 0) {
                cost += a[trace[i - 1]][trace[i]];
            }
        }
        space.add(cost);
        result.add(arr);
    }

    public void BT(int line) {
        if (trace[line - 1] == endPoint) {
            Sol(line);
        } else {
            for (int i = 1; i <= size; i++) {
                if (a[trace[line - 1]][i] != 0) {
                    if (!visited[i]) {
                        visited[i] = true;
                        trace[line] = i;
                        BT(line + 1);
                        trace[line] = 0;
                        visited[i] = false;
                    }
                }
            }
        }
    }

    public String countPath() {
        if (count > 1)
            return "There are " + Integer.toString(count) + " paths";
        else
            return "There is " + Integer.toString(count) + " path";
    }

    public int getBeginPoint() {
        return beginPoint;
    }

    public void setBeginPoint(int beginPoint) {
        this.beginPoint = beginPoint;
    }

    public int getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(int endPoint) {
        this.endPoint = endPoint;
    }

    public int[][] getA() {
        return a;
    }

    public void setA(int[][] a) {
        this.a = a;
    }

    public int[] getP(int index) {
        return result.get(index);
    }

    public int getSpace(int index) {
        return space.get(index);
    }

    public ArrayList<Points> getArrPoints() {
        return arrPoints;
    }

    public void setArrPoints(ArrayList<Points> arrPoints) {
        this.arrPoints = arrPoints;
    }

    public ArrayList<Lines> getArrLines() {
        return arrLines;
    }

    public void setArrLines(ArrayList<Lines> arrLines) {
        this.arrLines = arrLines;
    }

    public boolean isMapType() {
        return mapType;
    }

    public void setMapType(boolean mapType) {
        this.mapType = mapType;
    }

    public int getSize() {
        return size;
    }

    public int getLine() {
        return line - 1;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
