package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;

public class Frame extends JFrame implements ActionListener {

	private JFrame frameAbout, frameHelp;
	private String data[][], head[];
	private JComboBox<String> cbbBeginPoint = new JComboBox<String>();
	private JComboBox<String> cbbEndPoint = new JComboBox<String>();
	private JComboBox<String> cbbChoosePath = new JComboBox<String>();
	private JComboBox<String> cbbGraphDemo = new JComboBox<String>();

	private JRadioButton radUndirected, radDirected;
	private JButton btnRunAll, btnRandom, btnRunTry, btnNext, btnPrev;

	private JTable tableLog;

	// draw
	private JPanel drawPanel = new JPanel();
	private JButton btnPoint, btnLine, btnUpdate, btnMove, btnOpen, btnSaveTxt, btnSavePng, btnNew, btnBfs, btnBfsNext;
	// graph
	private Draws draw = new Draws();

	// log
	private JTextArea textLog;

	private Popups popup;

	// algo
	private int maxsize;
	private int index;
	private ArrayList<Integer> updateTrace;

	private int indexBeginPoint = 0, indexEndPoint = 0;
	private boolean mapType = false;

	int WIDTH_SELECT, HEIGHT_SELECT;

	Dijkstra dijkstra = new Dijkstra();
	Try Try = new Try();
	Algorithm algo = new Algorithm();

	public Frame(String title) {
		setTitle(title);
		setLayout(new BorderLayout(5, 5));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// addMenu
		add(creatMenu(), BorderLayout.PAGE_START);
		add(creatSelectPanel(), BorderLayout.EAST);
		add(creatPaintPanel(), BorderLayout.CENTER);
		JMenuBar mnb = new JMenuBar();
		add(mnb, BorderLayout.PAGE_END);

		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		// set icon
		URL iconURL = getClass().getResource("/assets/iconMain.png");
		ImageIcon icon = new ImageIcon(iconURL);
		setIconImage(icon.getImage());

	}

	private JMenuBar creatMenu() {

		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);

		menuFile.add(createMenuItem("New", KeyEvent.VK_N, 2));
		menuFile.add(createMenuItem("Open", KeyEvent.VK_O, 2));
		menuFile.add(createMenuItem("Save PNG", KeyEvent.VK_S, 2));
		menuFile.add(createMenuItem("Save TXT", KeyEvent.VK_T, 2));
		menuFile.addSeparator();
		menuFile.add(createMenuItem("Exit", KeyEvent.VK_X, 2));

		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_H);
		menuHelp.add(createMenuItem("Help", KeyEvent.VK_H, 2));
		menuHelp.add(createMenuItem("About", KeyEvent.VK_A, 2));

		JMenuBar menuBar = new JMenuBar();
		menuBar.add(menuFile);
		menuBar.add(menuHelp);
		return menuBar;
	}

	private JPanel creatSelectPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel panelTop = new JPanel(new GridLayout(7, 1, 5, 5));
		JPanel panelBottom = new JPanel(new BorderLayout());

		JPanel panelMapTypeTemp = new JPanel(new GridLayout(1, 2, 5, 5));
		panelMapTypeTemp.setBorder(new EmptyBorder(0, 10, 0, 5));
		panelMapTypeTemp.add(radUndirected = createRadioButton("Undirected", true));
		panelMapTypeTemp.add(radDirected = createRadioButton("Directed", false));
		ButtonGroup groupMapType = new ButtonGroup();
		groupMapType.add(radUndirected);
		groupMapType.add(radDirected);
		JPanel panelMapType = new JPanel(new BorderLayout());
		panelMapType.setBorder(new TitledBorder("Map Type"));
		panelMapType.add(panelMapTypeTemp, BorderLayout.PAGE_START);

		JPanel panelInputRandom = new JPanel(new BorderLayout());
		panelInputRandom.setBorder(new TitledBorder("Input Random"));
		btnRandom = new JButton("Random");
		panelInputRandom.add(btnRandom, BorderLayout.PAGE_START);
		btnRandom.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				drawDemo();

			}
		});

		JPanel panelSelectPointTemp = new JPanel(new GridLayout(1, 2, 20, 5));
		panelSelectPointTemp.setBorder(new EmptyBorder(0, 15, 0, 5));
		panelSelectPointTemp.add(cbbBeginPoint = createComboxBox("Begin"));
		panelSelectPointTemp.add(cbbEndPoint = createComboxBox("End"));
		JPanel panelSelectPoint = new JPanel(new BorderLayout());
		panelSelectPoint.setBorder(new TitledBorder("Point"));
		panelSelectPoint.add(panelSelectPointTemp, BorderLayout.PAGE_START);

		JPanel panelNextPrev = new JPanel(new GridLayout(1, 3, 5, 5));
		panelNextPrev.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelNextPrev.add(btnPrev = createButton("Prev"));
		panelNextPrev.add(cbbChoosePath = createComboxBox("Path"));
		panelNextPrev.add(btnNext = createButton("Next"));
		JPanel panelNextPrevBox = new JPanel(new BorderLayout());
		panelNextPrevBox.setBorder(new TitledBorder("Select path"));
		panelNextPrevBox.add(panelNextPrev);

		JPanel panelRun = new JPanel(new BorderLayout(2, 2));
		panelRun.add(btnRunTry = createButton("Run"), BorderLayout.PAGE_START);
		panelRun.add(panelNextPrev, BorderLayout.PAGE_END);
		panelRun.setBorder(new TitledBorder("Run"));

		JPanel panelAlgo = new JPanel(new GridLayout(1, 2, 5, 5));
		panelAlgo.setBorder(new EmptyBorder(0, 0, 0, 0));
		// panelAlgo.add(btnDfs = createButton("DFS"));
		panelAlgo.add(btnBfs = createButton("BFS"));
		panelAlgo.add(btnRunAll = createButton("Dijkstra"));
		JPanel panelAlgorithm = new JPanel(new BorderLayout(2, 6));
		panelAlgorithm.setBorder(new TitledBorder("Algorithm"));
		panelAlgorithm.add(panelAlgo, BorderLayout.PAGE_START);
		btnBfsNext = new JButton("Next");
		JButton btnFake1 = new JButton("  ");
		JButton btnFake2 = new JButton("  ");
		JPanel panelFake = new JPanel(new GridLayout(1, 3, 5, 5));
		panelFake.add(btnFake1);
		panelFake.add(btnBfsNext);
		panelFake.add(btnFake2);
		panelAlgorithm.add(panelFake, BorderLayout.CENTER);
		btnFake1.setVisible(false);
		btnFake2.setVisible(false);
		btnBfsNext.setVisible(false);

		panelTop.add(panelMapType);
		panelTop.add(panelInputRandom);
		panelTop.add(panelSelectPoint);
		panelTop.add(panelRun);
		panelTop.add(panelAlgorithm);
		panelTop.add(creatLogPanel());

		panel.add(panelTop, BorderLayout.PAGE_START);
		panel.add(panelBottom, BorderLayout.CENTER);
		panel.setBorder(new EmptyBorder(0, 5, 0, 0));
		WIDTH_SELECT = (int) panel.getPreferredSize().getWidth();
		HEIGHT_SELECT = (int) panel.getPreferredSize().getHeight();
		return panel;
	}

	private JPanel creatPaintPanel() {
		drawPanel.setLayout(new BoxLayout(drawPanel, BoxLayout.Y_AXIS));
		drawPanel.setBorder(new TitledBorder(""));
		drawPanel.setBackground(null);
		Icon icon;
		// String link = File.separator + "icon" + File.separator;
		String link = "/assets/";

		icon = getIcon(link + "iconNew.png");
		drawPanel.add(btnNew = createButtonImage(icon, "New graph"));

		icon = getIcon(link + "iconOpen.png");
		drawPanel.add(btnOpen = createButtonImage(icon, "Open graph"));

		icon = getIcon(link + "iconSavePng.png");
		drawPanel.add(btnSavePng = createButtonImage(icon, "Save graph PNG"));

		icon = getIcon(link + "iconSaveTxt.png");
		drawPanel.add(btnSaveTxt = createButtonImage(icon, "Save graph TXT"));

		icon = getIcon(link + "iconPoint.png");
		drawPanel.add(btnPoint = createButtonImage(icon, "Draw Point"));

		icon = getIcon(link + "iconLine.png");
		drawPanel.add(btnLine = createButtonImage(icon, "Draw line"));

		icon = getIcon(link + "iconMove.png");
		drawPanel.add(btnMove = createButtonImage(icon, "Move Point"));

		icon = getIcon(link + "iconOk.png");
		drawPanel.add(btnUpdate = createButtonImage(icon, "Update Graph"));

		popup = createpopup();
		draw.setComponentPopupMenu(popup);

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		panel.add(drawPanel, BorderLayout.WEST);
		panel.add(draw, BorderLayout.CENTER);
		return panel;
	}

	private ImageIcon getIcon(String link) {
		return new ImageIcon(getClass().getResource(link));
	}

	private JPanel creatLogPanel() {
		textLog = new JTextArea("Group 10");
		textLog.setRows(3);
		textLog.setEditable(false);
		JScrollPane scrollPath = new JScrollPane(textLog);
		@SuppressWarnings("unused")
		JScrollPane scroll = new JScrollPane(tableLog = createTable());
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(new TitledBorder("Log"));
		panel.add(scrollPath, BorderLayout.PAGE_START);
		panel.setPreferredSize(new Dimension(WIDTH_SELECT * 7 / 2, HEIGHT_SELECT / 5));
		return panel;
	}

	private JMenuItem createMenuItem(String title, int keyEvent, int event) {
		JMenuItem mi = new JMenuItem(title);
		mi.setMnemonic(keyEvent);
		mi.setAccelerator(KeyStroke.getKeyStroke(keyEvent, event));
		mi.addActionListener(this);
		return mi;
	}

	private Popups createpopup() {
		Popups popup = new Popups();
		popup.add(createMenuItem("Change cost", 0, 0));
		popup.add(createMenuItem("Delete", 0, 0));
		return popup;
	}

	// create radioButton on group btnGroup and add to panel
	private JRadioButton createRadioButton(String lable, Boolean select) {
		JRadioButton rad = new JRadioButton(lable);
		rad.addActionListener(this);
		rad.setSelected(select);
		return rad;
	}

	// create button and add to panel
	private JButton createButton(String lable) {
		JButton btn = new JButton(lable);
		btn.addActionListener(this);
		return btn;
	}

	// create buttonImage and add to panel
	private JButton createButtonImage(Icon icon, String toolTip) {
		JButton btn = new JButton(icon);
		btn.setMargin(new Insets(0, 0, 0, 0));
		btn.addActionListener(this);
		btn.setToolTipText(toolTip);
		return btn;
	}

	// create comboBox and add to panel
	private JComboBox<String> createComboxBox(String title) {
		String list[] = { title };
		JComboBox<String> cbb = new JComboBox<String>(list);
		cbb.addActionListener(this);
		cbb.setEditable(false);
		cbb.setMaximumRowCount(5);
		return cbb;
	}

	private JTable createTable() {
		JTable table = new JTable();
		return table;
	}

	// ------------------ Action ------------------//

	private void actionUpdate() {
		updateListPoint();
		updateListPath();
		resetDataDijkstra();
		resetDataTry();
		setDrawResultOrStep(false);
		draw.setDrawTry(false);
		reDraw();
		clearLog();
	}

	private void actionDrawPoint() {
		draw.setDraw(1);
		setDrawResultOrStep(false);
		draw.setDrawTry(false);
	}

	private void actionDrawLine() {
		draw.setDraw(2);
		setDrawResultOrStep(false);
		draw.setDrawTry(false);
	}

	private void actionOpen() {
		File file = new File("");
		String currentDirectory = file.getAbsolutePath() + "\\src\\input";
		JFileChooser fc = new JFileChooser(new File(currentDirectory));
		fc.setDialogTitle("Open graph");
		int select = fc.showOpenDialog(this);
		if (select == 0) {
			String path = fc.getSelectedFile().toString();
			draw.readFile(path);
			textLog.setText("Done read.");
			actionUpdate();
		}
	}

	private void actionSave() {
		File file = new File("");
		String currentDirectory = file.getAbsolutePath() + "\\src\\output";
		JFileChooser fc = new JFileChooser(new File(currentDirectory));
		fc.setDialogTitle("Save graph");
		fc.setFileFilter(new FileFilter() {
			public String getDescription() {
				return "PNG Images (*.png)";
			}

			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				} else {
					String filename = f.getName().toLowerCase();
					return filename.endsWith(".png");
				}
			}
		});
		int select = fc.showSaveDialog(this);
		if (select == 0) {
			String path = fc.getSelectedFile().getPath() + ".png";
			try {
				Robot robot = new Robot();
				Rectangle capture = new Rectangle(draw.getLocationOnScreen().x, draw.getLocationOnScreen().y,
						draw.getWidth(), draw.getHeight());
				BufferedImage Image = robot.createScreenCapture(capture);
				ImageIO.write(Image, "png", new File(path));
				textLog.setText("Save successful");
				JOptionPane.showMessageDialog(null, "Save successful", "Save", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception ex) {
				System.out.println(ex);
				textLog.setText("Fault" + ex.toString());
			}
		}
	}

	private void actionSaveTxt() {
		File file = new File("");
		String currentDirectory = file.getAbsolutePath() + "\\src\\output";
		JFileChooser fc = new JFileChooser(new File(currentDirectory));
		fc.setDialogTitle("Save graph");
		fc.setFileFilter(new FileFilter() {
			public String getDescription() {
				return "Text File (*.txt)";
			}

			public boolean accept(File f) {
				if (f.isDirectory()) {
					return true;
				} else {
					String filename = f.getName().toLowerCase();
					return filename.endsWith(".txt");
				}
			}
		});
		int select = fc.showSaveDialog(this);
		if (select == 0) {
			String path = fc.getSelectedFile().getPath() + ".txt";
			try {
				FileWriter fw = new FileWriter(path);
				ArrayList<Points> arrPoints = new ArrayList<Points>();
				ArrayList<Lines> arrLines = new ArrayList<Lines>();
				arrPoints = draw.getData().getArrPoints();
				arrLines = draw.getData().getArrLines();
				fw.write(Integer.toString(arrPoints.size() - 1) + " " + Integer.toString(arrLines.size() - 1) + "\n");
				for (int i = 1; i < arrLines.size(); i++) {
					fw.write(Integer.toString(arrLines.get(i).getIndexPointA()) + " "
							+ Integer.toString(arrLines.get(i).getIndexPointB()) + " "
							+ Integer.toString(arrLines.get(i).getCost()) + "\n");
				}
				fw.close();
				textLog.setText("Save successful");
				JOptionPane.showMessageDialog(null, "Save successful", "Save", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception e) {
				System.out.println(e);
				textLog.setText("Fault" + e.toString());
			}
		}
	}

	private void actionNew() {
		setDrawResultOrStep(false);
		draw.setDrawResult(false);
		draw.setDrawStep(false);
		draw.setDrawTry(false);
		draw.setResetGraph(true);
		draw.repaint();
		draw.init();
		updateListPoint();
		updateListPath();
		clearLog();
	}

	private void actionChoosePoint() {
		draw.setDrawTry(false);
		resetDataDijkstra();
		resetDataTry();
		setDrawResultOrStep(false);
		reDraw();
		clearLog();
	}

	private void actionChoosePath() {
		int indexPath = cbbChoosePath.getSelectedIndex();
		String log = "Path " + indexPath + ": ";
		int arr[] = Try.getP(indexPath - 1);
		for (int i = 0; i < arr.length - 1; i++)
			log += arr[i] + " -> ";
		log += arr[arr.length - 1] + "\n";
		log += "Cost: " + Try.getSpace(indexPath - 1);
		textLog.setText(log);
		draw.setP(arr);
		setDrawResultOrStep(false);
		draw.repaint();
	}

	private void showDialogChangeCost() {
		int index = draw.indexLineContain(popup.getPoint());
		if (index > 0) {
			draw.changeCost(index);
			actionUpdate();
		} else {
			JOptionPane.showMessageDialog(null, "Haven't line seleced!");
		}
	}

	private void showDialogDelete() {
		int index = draw.indexPointContain(popup.getPoint());
		if (index <= 0) {
			index = draw.indexLineContain(popup.getPoint());
			if (index > 0) {
				// show message dialog
				Lines ml = draw.getData().getArrLines().get(index);
				String message = "Do you want delete the line from " + ml.getIndexPointA() + " to "
						+ ml.getIndexPointB();
				int select = JOptionPane.showConfirmDialog(this, message, "Delete line", JOptionPane.OK_CANCEL_OPTION);
				if (select == 0) {
					draw.deleteLine(index);
					actionUpdate();
				}
			} else {
				JOptionPane.showMessageDialog(null, "Haven't point or line seleced!");
			}
		} else {
			// show message dialog
			String message = "Do you want delete the point " + index;
			int select = JOptionPane.showConfirmDialog(this, message, "Delete point", JOptionPane.OK_CANCEL_OPTION);
			if (select == 0) {
				draw.deletePoint(index);
				actionUpdate();
			}
		}
	}

	private void updateListPath() {
		int size = Try.getCount() + 1;
		String listPoint[] = new String[size];
		listPoint[0] = "Path";
		for (int i = 1; i < listPoint.length; i++) {
			listPoint[i] = String.valueOf(i);
		}
		cbbChoosePath.setModel(new DefaultComboBoxModel<String>(listPoint));
		cbbChoosePath.setMaximumRowCount(5);
	}

	private void updateListPoint() {
		int size = draw.getData().getArrPoints().size();
		String listPoint[] = new String[size];
		listPoint[0] = "Begin";
		for (int i = 1; i < listPoint.length; i++) {
			listPoint[i] = String.valueOf(i);
		}

		cbbBeginPoint.setModel(new DefaultComboBoxModel<String>(listPoint));
		cbbBeginPoint.setMaximumRowCount(5);

		if (size > 1) {
			listPoint = new String[size + 1];
			listPoint[0] = "End";
			for (int i = 1; i < listPoint.length; i++) {
				listPoint[i] = String.valueOf(i);
			}
			listPoint[listPoint.length - 1] = "All";
		} else {
			listPoint = new String[1];
			listPoint[0] = "End";
		}

		cbbEndPoint.setModel(new DefaultComboBoxModel<String>(listPoint));
		cbbEndPoint.setMaximumRowCount(5);
	}

	private void setEnableDraw(boolean check, String matrix) {
		cbbGraphDemo.setEnabled(!check);
	}

	private void setEnableMapType(boolean mapType) {
		updateListPoint();
		updateListPath();
		clearLog();
		this.mapType = mapType;
		draw.setTypeMap(mapType);
		setDrawResultOrStep(false);
		draw.setDrawTry(false);
		draw.repaint();
		resetDataDijkstra();
		resetDataTry();
	}

	private void setDrawResultOrStep(boolean check) {
		draw.setDrawResult(check);
		draw.setDrawStep(check);
	}

	private void resetDataTry() {
		Try = new Try();
		Try.setMapType(mapType);
		Try.setArrPoints(draw.getData().getArrPoints());
		Try.setArrLines(draw.getData().getArrLines());
		Try.input();
	}

	private void resetDataDijkstra() {
		dijkstra = new Dijkstra();
		dijkstra.setMapType(mapType);
		dijkstra.setArrPoints(draw.getData().getArrPoints());
		dijkstra.setArrLines(draw.getData().getArrLines());
		dijkstra.input();
		dijkstra.processInput();
	}

	private void reDraw() {
		draw.setReDraw(true);
		draw.repaint();
	}

	private void clearLog() {
		DefaultTableModel model = new DefaultTableModel();
		tableLog.setModel(model);
		clearPath();
	}

	private void clearPath() {
		textLog.setText("Group 10");
	}

	private void loadLog(boolean isStep) {
		int infinity = dijkstra.getInfinity();
		int logLen[][] = dijkstra.getLogLen();
		int logP[][] = dijkstra.getLogP();
		head = new String[logLen.length - 1];
		data = new String[dijkstra.getNumberPointChecked()][logLen.length - 1];
		boolean check[] = new boolean[logLen.length - 1];

		for (int i = 0; i < logLen.length - 1; i++) {
			head[i] = String.valueOf(i + 1);
			check[i] = false;
			data[0][i] = "[∞, ∞]";
		}

		data[0][indexBeginPoint - 1] = "[0, " + indexBeginPoint + "]";

		for (int i = 1; i < data.length; i++) {
			int min = infinity, indexMin = -1;
			// // check "*" for min len
			for (int j = 1; j < logLen.length; j++) {
				if (min > logLen[i][j] && !check[j - 1]) {
					min = logLen[i][j];
					indexMin = j - 1;
				}
			}
			if (indexMin > -1) {
				check[indexMin] = true;
			}

			for (int j = 1; j < logLen.length; j++) {

				if (min > logLen[i][j] && !check[j - 1]) {
					min = logLen[i][j];
					indexMin = j - 1;
				}

				String p = "∞";
				if (logP[i][j] > 0) {
					p = logP[i][j] + "";
				}
				if (check[j - 1]) {
					data[i][j - 1] = "-";
				} else if (logLen[i][j] == infinity) {
					data[i][j - 1] = "[∞, " + p + "]";
				} else {
					data[i][j - 1] = "[" + logLen[i][j] + ", " + p + "]";
				}
			}

			if (indexMin > -1) {
				data[i - 1][indexMin] = "*" + data[i - 1][indexMin];
			}
		}

		// check "*" for min len of row last
		int min = infinity, indexMin = -1;
		for (int j = 1; j < logLen.length; j++) {
			if (min > logLen[data.length - 1][j] && !check[j - 1]) {
				min = logLen[data.length - 1][j];
				indexMin = j - 1;
			}
		}
		if (indexMin > -1) {
			check[indexMin] = true;
			data[data.length - 1][indexMin] = "*" + data[data.length - 1][indexMin];
		}
	}

	private void drawDemo() {
		Random rd = new Random();
		int demo = rd.nextInt(7);
		if (demo < 1 || demo > 6)
			demo = 1;
		draw.readDemo(demo);
		actionUpdate();
	}

	private boolean checkRun() {
		int size = draw.getData().getArrPoints().size() - 1;
		indexBeginPoint = cbbBeginPoint.getSelectedIndex();
		indexEndPoint = cbbEndPoint.getSelectedIndex();
		if (indexEndPoint == size + 1) { // all Point
			indexEndPoint = -1;
		}

		if (size < 1 || indexBeginPoint == 0 || indexEndPoint == 0) {
			JOptionPane.showMessageDialog(null, "Error chose points or don't Update graph to chose points", "Error",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	private boolean checkRunAlgo() {
		int size = draw.getData().getArrPoints().size() - 1;
		indexBeginPoint = cbbBeginPoint.getSelectedIndex();

		if (size < 1 || indexBeginPoint == 0) {
			JOptionPane.showMessageDialog(null, "Error chose points or don't Update graph to chose points", "Error",
					JOptionPane.WARNING_MESSAGE);
			return false;
		}
		return true;
	}

	private void setBeginEndPoint() {
		draw.setIndexBeginPoint(indexBeginPoint);
		draw.setIndexEndPoint(indexEndPoint);
		dijkstra.setBeginPoint(indexBeginPoint);
		dijkstra.setEndPoint(indexEndPoint);
	}

	private void runAll() {
		if (checkRun()) {
			resetDataDijkstra();
			setBeginEndPoint();
			dijkstra.dijkstra();
			textLog.setText(dijkstra.tracePath());
			loadLog(false);

			draw.setDrawStep(false);
			draw.setDrawResult(true);
			draw.setDrawTry(false);
			draw.setA(dijkstra.getA());
			draw.setP(dijkstra.getP());
			draw.setInfinity(dijkstra.getInfinity());
			draw.setLen(dijkstra.getLen());
			draw.setCheckedPointMin(dijkstra.getCheckedPointMin());
			draw.repaint();
		}
	}

	private void runTry() {
		if (checkRun()) {
			Try.setMapType(mapType);
			Try.setArrPoints(draw.getData().getArrPoints());
			Try.setArrLines(draw.getData().getArrLines());
			draw.setIndexBeginPoint(indexBeginPoint);
			draw.setIndexEndPoint(indexEndPoint);
			Try.setBeginPoint(indexBeginPoint);
			Try.setEndPoint(indexEndPoint);
			Try.input();
			if (indexBeginPoint != indexEndPoint)
				Try.BT(1);
			JOptionPane.showMessageDialog(null, Try.countPath(), "Log", JOptionPane.INFORMATION_MESSAGE);

			draw.setDrawStep(false);
			draw.setDrawResult(false);
			draw.setDrawTry(true);
			draw.setA(Try.getA());
			updateListPath();
			if (Try.getCount() > 0) {
				draw.setP(Try.getP(0));
				cbbChoosePath.setSelectedIndex(1);
			} else {
				textLog.setText("Can't go!");
			}
			draw.setCount(Try.getCount());
			draw.repaint();
		}
	}

	private void runBfs() {
		if (checkRunAlgo()) {
			setDrawResultOrStep(false);
			draw.setDrawTry(false);
			reDraw();
			clearLog();
			algo.setMapType(mapType);
			algo.setArrPoints(draw.getData().getArrPoints());
			algo.setArrLines(draw.getData().getArrLines());
			draw.setIndexBeginPoint(indexBeginPoint);
			draw.setIndexEndPoint(indexEndPoint);
			algo.setBeginPoint(indexBeginPoint);
			algo.input();
			algo.BFS();
			updateTrace = new ArrayList<Integer>();
			maxsize = algo.getMaxsize();
			index = 0;
			btnBfsNext.setVisible(true);
			btnBfsNext.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (index < maxsize) {
						draw.setDrawStep(true);
						draw.setDrawResult(false);
						draw.setDrawTry(false);
						draw.setA(algo.getA());
						draw.setDad(algo.getDad());
						int arr[] = algo.getP(index);
						draw.setP(arr);
						String log = "Step: " + (index + 1) + "\nEdge: ";
						for (int i = 0; i < arr.length; i++) {
							updateTrace.add(arr[i]);
							log += arr[i] + " ";
						}
						textLog.setText(log);
						draw.setTrace(updateTrace);
						draw.repaint();
						index++;
					} else {
						index = 0;
						draw.setDrawStep(false);
						draw.setDrawResult(false);
						draw.setDrawTry(false);
						JOptionPane.showMessageDialog(null, "Done BFS", "Log", JOptionPane.INFORMATION_MESSAGE);
						btnBfsNext.setVisible(false);
						btnBfsNext.removeActionListener(this);
					}
				}
			});
		}
	}

	private void increasePath() {
		int currentIndex = cbbChoosePath.getSelectedIndex();
		if (currentIndex == Try.getCount())
			return;
		int indexPath = currentIndex + 1;
		cbbChoosePath.setSelectedIndex(indexPath);
		String log = "Path " + indexPath + ": ";
		int arr[] = Try.getP(indexPath - 1);
		for (int i = 0; i < arr.length - 1; i++)
			log += arr[i] + " -> ";
		log += arr[arr.length - 1] + "\n";
		log += "Cost: " + Try.getSpace(indexPath - 1);
		textLog.setText(log);
		draw.setP(arr);
		setDrawResultOrStep(false);
		draw.repaint();
	}

	private void reductionPath() {
		int currentIndex = cbbChoosePath.getSelectedIndex();
		if (currentIndex == 0)
			return;
		int indexPath = currentIndex - 1;
		cbbChoosePath.setSelectedIndex(indexPath);
		String log = "Path " + indexPath + ": ";
		int arr[] = Try.getP(indexPath - 1);
		for (int i = 0; i < arr.length - 1; i++)
			log += arr[i] + " -> ";
		log += arr[arr.length - 1] + "\n";
		log += "Cost: " + Try.getSpace(indexPath - 1);
		textLog.setText(log);
		draw.setP(arr);
		setDrawResultOrStep(false);
		draw.repaint();
	}

	private void showHelp() {
		if (frameHelp == null) {
			frameHelp = new HelpAndAbout(0, "Help");
		}
		frameHelp.setVisible(true);
	}

	private void showAbout() {
		if (frameAbout == null) {
			frameAbout = new HelpAndAbout(1, "About");
		}
		frameAbout.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();

		// select button in paint
		if (e.getSource() == btnUpdate) {
			actionUpdate();
		}

		if (e.getSource() == btnPoint) {
			actionDrawPoint();
		}

		if (e.getSource() == btnLine) {
			actionDrawLine();
		}
		if (e.getSource() == btnMove) {
			draw.setDraw(3);
		}

		if (e.getSource() == btnNew) {
			actionNew();
		}

		// select input method
		if (command == "Draw") {
			setEnableDraw(true, "outputMatrix");
		} else if (command == "Matrix") {
			setEnableDraw(true, "inputMatrix");
		} else if (command == "Demo") {
			setEnableDraw(false, "outputMatrix");
			drawDemo();
		}

		// select Map type
		if (e.getSource() == radUndirected) {
			setEnableMapType(false);
		} else if (e.getSource() == radDirected) {
			setEnableMapType(true);
		}

		if (e.getSource() == cbbGraphDemo) {
			drawDemo();
		}

		// select point
		if (e.getSource() == cbbBeginPoint || e.getSource() == cbbEndPoint) {
			actionChoosePoint();
		}

		// select path
		if (e.getSource() == cbbChoosePath) {
			actionChoosePath();
		}

		if (e.getSource() == btnPrev) {
			reductionPath();
		}

		if (e.getSource() == btnNext) {
			increasePath();
		}

		// select run

		if (e.getSource() == btnRunAll) {
			runAll();
		}

		// if (e.getSource() == btnDfs) {
		// runDfs();
		// }

		if (e.getSource() == btnBfs) {
			runBfs();
		}

		if (e.getSource() == btnRunTry) {
			runTry();
		}
		// select menu bar
		if (command == "New") {
			actionNew();
		}
		if (command == "Open" || e.getSource() == btnOpen) {
			actionOpen();
		}
		if (command == "Save PNG" || e.getSource() == btnSavePng) {
			actionSave();
		}
		if (command == "Save TXT" || e.getSource() == btnSaveTxt) {
			actionSaveTxt();
		}
		if (command == "Exit") {
			System.exit(0);
		}
		if (command == "About") {
			showAbout();
		}
		if (command == "Help") {
			showHelp();
		}

		// select popup menu
		if (command == "Change cost") {
			showDialogChangeCost();
		}
		if (command == "Delete") {
			showDialogDelete();
		}
	}

}
