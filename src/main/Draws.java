package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Draws extends JPanel implements MouseListener, MouseMotionListener {
	private Data data = new Data();
	private ArrayList<Integer> arrPointResultStep = new ArrayList<Integer>();
	private int len[];
	private int a[][];
	private int p[];
	private ArrayList<Integer> trace;
	private int dad[];
	private int infinity;
	private int r = Config.R;
	private int d = Config.D;
	private int x = 0, y = 0;
	private int indexPointBeginLine, indexPointEndLine, indexTemp;
	private Point pointBeginLine;
	private Point point;
	boolean checkDrawLine = false, isFindPoint = true;
	private int draw = 0;
	private Color colorBackGround = Config.colorBackGround;
	private Color colorCost = Config.colorCost;
	private Color colorIndex = Config.colorIndex;
	private Color colorDraw = Config.colorDraw;
	private Color colorResult = Config.colorResult;
	private Color colorBegin = Config.colorBegin;
	private Color colorEnd = Config.colorEnd;
	private Color colorLine = Config.colorLine;
	private int sizeLine = 1, sizeLineResult = 2;
	private boolean drawResult = false;
	private boolean drawStep = false;
	private boolean drawTry = false;
	private boolean reDraw = false;
	private boolean resetGraph = false;
	private boolean typeMap = false;
	private boolean checkedPointMin[];
	private int indexBeginPoint, indexEndPoint;
	private int drawWith, drawHeight;
	private int count;
	private Point centerPoint;
	private int R;

	public Draws() {
		init();
		addMouseMotionListener(this);
		addMouseListener(this);
	}

	public void init() {
		data.getArrLines().clear();
		data.getArrPoints().clear();
		Points p0 = new Points(new Ellipse2D.Float(50, 50, 50, 50));
		data.getArrPoints().add(p0);
		data.getArrLines().add(new Lines(creatLine(p0.getPoint(), p0.getPoint()), 0, 0, -1));
	}

	// main draw
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(colorBackGround);
		Graphics2D g2d = (Graphics2D) g;
		// draw line
		reDraw(g2d, false);

		// draw result dijjkstra
		if (drawResult) {
			if (indexEndPoint == -1) {
				drawResultAllPoint(g2d);
			} else {
				drawResult(g2d);
			}
			// drawResult = false;
		}

		// draw Step dijjkstra
		if (drawStep) {
			drawResultStep(g2d);
			// drawStep = false;
		}

		// draw result Try
		if (drawTry) {
			drawTryPath(g2d);
		}

		// redraw the begin graph
		if (reDraw) {
			reDraw(g2d, true);
			reDraw = false;
		}

		// reset graph to graph space
		if (resetGraph) {
			resetGraph(g2d);
			init();
			resetGraph = false;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) { // click
		x = e.getX();
		y = e.getY();
		if (draw == 1) { // draw point
			Ellipse2D.Float el = new Ellipse2D.Float(x - r, y - r, d, d);
			Points point = new Points(el);
			data.getArrPoints().add(point);
			repaint();
		}
		// mouse right
		if (e.getButton() == MouseEvent.BUTTON3) {
			System.out.println("Right Clicked");
			isRightClick = true;
			pointRight = e.getPoint();
		}
	}

	@Override
	// on click
	public void mousePressed(MouseEvent e) {
		pointBeginLine = e.getPoint();
		point = e.getPoint();
		e.getPoint();
		e.getPoint();
		data.getArrPoints().get(indexTemp).getEllipse().x = e.getX() - r;
		data.getArrPoints().get(indexTemp).getEllipse().y = e.getY() - r;
	}

	@Override
	// out click
	public void mouseReleased(MouseEvent e) {
		boolean drawAgaine = false;
		if (checkDrawLine) {
			indexPointEndLine = indexPointContain(new Point(e.getX(), e.getY()));
			if (indexPointEndLine > 0) {
				isFindPoint = false;
			}

			for (int i = 1; i < data.getArrLines().size(); i++) {
				Lines line = data.getArrLines().get(i);
				if (typeMap) { // directed
					// draw again <=> change cost
					if (line.getIndexPointA() == indexPointBeginLine && line.getIndexPointB() == indexPointEndLine) {
						drawAgaine = true;
						break;
					} // draw line reverse <=> not change cost
					else if (line.getIndexPointA() == indexPointEndLine
							&& line.getIndexPointB() == indexPointBeginLine) {
						addLineToList(indexPointBeginLine, indexPointEndLine, line.getCost());
						drawAgaine = true;
						break;
					}
				} else { // undirected
					// draw again <=> change cost
					if ((line.getIndexPointA() == indexPointBeginLine && line.getIndexPointB() == indexPointEndLine)
							|| (line.getIndexPointA() == indexPointEndLine
									&& line.getIndexPointB() == indexPointBeginLine)) {
						drawAgaine = true;
						break;
					}
				}
			}
			if (!drawAgaine) {
				int cost = showDialogCost(indexPointBeginLine, indexPointEndLine);
				addLineToList(indexPointBeginLine, indexPointEndLine, cost);
			}
			checkDrawLine = false;
		}
		data.getArrLines().get(indexTemp).setIndexPointA(data.getArrLines().get(indexTemp).getIndexPointB());
		updateLine();
		repaint();// clear wrong line
		isFindPoint = true; // accept find first point
	}

	@Override
	public void mouseEntered(MouseEvent e) { // in frame
		// System.out.println("Entered");
	}

	@Override
	public void mouseExited(MouseEvent e) { // out frame
		// System.out.println("Exited");
	}

	@Override
	public void mouseDragged(MouseEvent e) { // move mouse
		if (isFindPoint) { // find point is true
			indexPointBeginLine = indexPointContain(pointBeginLine);
			if (indexPointBeginLine > 0) {
				isFindPoint = false;
			}
		}
		// drawing line or point
		if (draw == 2 || draw == 1 || indexPointBeginLine >= 0) {
			int dx = e.getX() - point.x;
			int dy = e.getY() - point.y;
			// move point
			if ((draw == 1 || draw == 3) && indexPointBeginLine > 0) {
				Ellipse2D.Float el = data.getArrPoints().get(indexPointBeginLine).getEllipse();

				el.x += dx;
				el.y += dy;
				data.getArrPoints().get(indexPointBeginLine).setEllipse(el);
			}
			// draw line
			if (draw == 2 && indexPointBeginLine >= 0) {
				checkDrawLine = true;
				data.getArrLines().get(indexTemp).setIndexPointA(indexPointBeginLine);
				Ellipse2D.Float el = data.getArrPoints().get(indexTemp).getEllipse();
				el.x += dx;
				el.y += dy;
				data.getArrPoints().get(indexTemp).setEllipse(el);
			}
			updateLine();
			repaint();
			point.x += dx;
			point.y += dy;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	// find index Point in list Point
	protected int indexPointContain(Point point) {
		for (int i = 1; i < data.getArrPoints().size(); i++) {
			if (data.getArrPoints().get(i).getEllipse().getBounds2D().contains(point)) {
				return i;
			}
		}
		return -1;
	}

	// find index Line in list Line
	protected int indexLineContain(Point point) {
		for (int i = 1; i < data.getArrLines().size(); i++) {
			if (data.getArrLines().get(i).containerPoint(point)) {
				return i;
			}
		}
		return -1;
	}

	protected int getIndexLine(int pA, int pB) {
		for (int i = 0; i <= data.getArrLines().size(); i++) {

			if (data.getArrLines().get(i).getIndexPointA() == pA && data.getArrLines().get(i).getIndexPointB() == pB
					|| data.getArrLines().get(i).getIndexPointB() == pA
							&& data.getArrLines().get(i).getIndexPointA() == pB) {
				return i;
			}
		}
		return -1;
	}

	// show dialog input cost
	protected int showDialogCost(int indexPointBeginLine, int indexPointEndLine) {
		int cost = 0;
		if (indexPointEndLine > 0 && indexPointEndLine < data.getArrPoints().size()
				&& indexPointEndLine != indexPointBeginLine) {
			String c = null;
			boolean ok = false;
			while (!ok) {
				try {
					c = JOptionPane.showInputDialog(null,
							"Input Cost from " + indexPointBeginLine + " to " + indexPointEndLine, "Change cost",
							1);
					cost = Integer.parseInt(c);
					if (cost > 0) {
						return cost;
					}
				} catch (NumberFormatException ex) { // input error number
				}
				// cancel
				if (c == null)
					break;
			}
		}
		return cost;
	}

	// Add line to list line
	protected void addLineToList(int indexPointBeginLine, int indexPointEndLine, int cost) {
		if (cost > 0) {
			Lines ml = new Lines(
					creatLine(data.getArrPoints().get(indexPointBeginLine).getPoint(),
							data.getArrPoints().get(indexPointEndLine).getPoint()),
					indexPointBeginLine, indexPointEndLine, cost);
			data.getArrLines().add(ml);
			repaint();
		}
	}

	// change cost
	protected void changeCost(int indexLine) {
		int cost = showDialogCost(data.getArrLines().get(indexLine).getIndexPointA(),
				data.getArrLines().get(indexLine).getIndexPointB());
		if (cost > 0) {
			data.getArrLines().get(indexLine).setCost(cost);
			for (int i = 1; i < data.getArrLines().size(); i++) {
				if (data.getArrLines().get(i).getIndexPointA() == data.getArrLines().get(indexLine).getIndexPointB()
						&& data.getArrLines().get(i).getIndexPointB() == data.getArrLines().get(indexLine)
								.getIndexPointA()) {
					data.getArrLines().get(i).setCost(cost);
					break;
				}
			}
			repaint();
		}
	}

	// delete line
	protected void deleteLine(int indexLine) {
		data.getArrLines().remove(indexLine);
	}

	// delete point
	protected void deletePoint(int indexPoint) {
		for (int i = 1; i < data.getArrLines().size(); i++) {
			int a = data.getArrLines().get(i).getIndexPointA();
			int b = data.getArrLines().get(i).getIndexPointB();
			// delete line of indexPoint
			if (a == indexPoint || b == indexPoint) {
				data.getArrLines().remove(i);
				i--;
			} else { // down line have indexPointA or indexPointB > indexPoint
				if (a > indexPoint) {
					data.getArrLines().get(i).setIndexPointA(a - 1);
				}
				if (b > indexPoint) {
					data.getArrLines().get(i).setIndexPointB(b - 1);
				}
			}
		}
		// delete point have indexPoint
		data.getArrPoints().remove(indexPoint);
	}

	// create line
	private Line2D.Double creatLine(Point p1, Point p2) {
		Line2D.Double l = new Line2D.Double(p1.x, p1.y, p2.x, p2.y);
		return l;
	}

	// update location line after move point
	private void updateLine() {
		for (int i = 0; i < data.getArrLines().size(); i++) {
			data.getArrLines().get(i)
					.setLine(creatLine(data.getArrPoints().get(data.getArrLines().get(i).getIndexPointA()).getPoint(),
							data.getArrPoints().get(data.getArrLines().get(i).getIndexPointB()).getPoint()));
		}
	}

	public void resetGraph(Graphics2D g2d) {
		g2d.setColor(colorBackGround);
		g2d.fillRect(0, 0, 600, 600);
	}

	private void reDraw(Graphics2D g2d, boolean checkReDraw) {
		resetGraph(g2d);
		for (int i = 0; i < data.getArrLines().size(); i++) {
			data.getArrLines().get(i).drawLine(g2d,
					data.getArrPoints().get(data.getArrLines().get(i).getIndexPointA()).getPoint(),
					data.getArrPoints().get(data.getArrLines().get(i).getIndexPointB()).getPoint(), colorCost,
					colorLine,
					sizeLine, typeMap);
		}

		// draw point
		for (int i = 1; i < data.getArrPoints().size(); i++) {
			data.getArrPoints().get(i).draw(g2d, i, colorDraw, colorIndex);
		}
	}

	private void drawResult(Graphics2D g2d) {
		if (checkedPointMin[indexEndPoint]) {
			String cost;
			int i = indexEndPoint;
			while (i != indexBeginPoint) {
				cost = String.valueOf(len[i]);
				Lines ml = new Lines(
						creatLine(data.getArrPoints().get(p[i]).getPoint(), data.getArrPoints().get(i).getPoint()), i,
						p[i],
						a[p[i]][i]);

				ml.drawLine(g2d, data.getArrPoints().get(p[i]).getPoint(), data.getArrPoints().get(i).getPoint(),
						colorCost,
						colorResult, sizeLineResult, typeMap);

				data.getArrPoints().get(i).drawResult(g2d, i, colorResult, colorIndex, cost, colorResult);

				i = p[i];
			}

			cost = "";
			data.getArrPoints().get(indexBeginPoint).drawResult(g2d, indexBeginPoint, colorBegin, colorIndex, cost,
					colorBegin);
			cost = String.valueOf(len[indexEndPoint]);
			data.getArrPoints().get(indexEndPoint).drawResult(g2d, indexEndPoint, colorEnd, colorIndex, cost,
					colorEnd);
		}
	}

	private void drawResultAllPoint(Graphics2D g2d) {
		int size = data.getArrPoints().size() - 1;
		String cost;
		for (int i = 1; i <= size; i++) {
			if (i != indexBeginPoint && a[p[i]][i] < infinity && p[i] > 0) {
				cost = len[i] + "";
				Lines ml = new Lines(
						creatLine(data.getArrPoints().get(p[i]).getPoint(), data.getArrPoints().get(i).getPoint()),
						i,
						p[i],
						a[p[i]][i]);

				ml.drawLine(g2d, data.getArrPoints().get(p[i]).getPoint(), data.getArrPoints().get(i).getPoint(),
						colorCost,
						colorResult, sizeLineResult, typeMap);

				data.getArrPoints().get(i).drawResult(g2d, i, colorResult, colorIndex, cost, colorResult);
			}

		}

		cost = "";
		data.getArrPoints().get(indexBeginPoint).drawResult(g2d, indexBeginPoint, colorBegin, colorIndex, cost,
				colorBegin);
	}

	private void drawTryPath(Graphics2D g2d) {
		if (count != 0 && indexBeginPoint != indexEndPoint) {
			int cost = 0;
			int size = p.length;
			for (int i = 0; i < size - 1; i++) {
				if (i != 0)
					cost += a[p[i - 1]][p[i]];
				Lines ml = new Lines(
						creatLine(data.getArrPoints().get(p[i]).getPoint(),
								data.getArrPoints().get(p[i + 1]).getPoint()),
						p[i], p[i + 1], a[p[i]][p[i + 1]]);
				ml.drawLine(g2d, data.getArrPoints().get(p[i]).getPoint(),
						data.getArrPoints().get(p[i + 1]).getPoint(),
						colorCost, colorResult, sizeLineResult, typeMap);

				if (i != 0)
					data.getArrPoints().get(p[i]).drawResult(g2d, p[i], colorResult, colorIndex,
							Integer.toString(cost), colorResult);
			}
			data.getArrPoints().get(p[0]).drawResult(g2d, p[0], colorBegin, colorIndex, "", colorBegin);
			cost += a[p[size - 2]][p[size - 1]];
			data.getArrPoints().get(p[size - 1]).drawResult(g2d, p[size - 1], colorEnd, colorIndex,
					Integer.toString(cost), colorEnd);
		}
	}

	private void drawResultStep(Graphics2D g2d) {
		int size = p.length;
		for (int i = 0; i < size; i++) {
			int u = p[i];
			while (dad[u] != 0) {
				Lines ml = new Lines(
						creatLine(data.getArrPoints().get(dad[u]).getPoint(), data.getArrPoints().get(u).getPoint()),
						dad[u],
						u, a[dad[u]][u]);
				ml.drawLine(g2d, data.getArrPoints().get(dad[u]).getPoint(), data.getArrPoints().get(u).getPoint(),
						colorCost,
						colorResult, sizeLineResult, typeMap);
				u = dad[u];
			}
		}
		for (int i : trace) {
			int u = i;
			while (dad[u] != 0) {
				Lines ml = new Lines(
						creatLine(data.getArrPoints().get(dad[u]).getPoint(), data.getArrPoints().get(u).getPoint()),
						dad[u],
						u, a[dad[u]][u]);
				ml.drawLine(g2d, data.getArrPoints().get(dad[u]).getPoint(), data.getArrPoints().get(u).getPoint(),
						colorCost,
						colorResult, sizeLineResult, typeMap);
				u = dad[u];
			}
		}
		for (int i : trace) {
			data.getArrPoints().get(i).drawResult(g2d, i, colorResult, colorIndex, "", colorResult);
		}
	}

	// create circle graph
	public void createGraph(int numberPoint) {
		centerPoint = new Point(getWidth() / 2, getHeight() / 2);
		init();
		R = (centerPoint.x > centerPoint.y) ? centerPoint.y : centerPoint.x;
		R = R * 5 / 6;
		for (int i = 1; i <= numberPoint; i++) {
			double phi = -90 + 360.0 * i / numberPoint;
			phi = phi * Math.PI / 180;
			int x = centerPoint.x + (int) (R * Math.cos(phi));
			int y = centerPoint.y + (int) (R * Math.sin(phi));

			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
	}

	public void createGraph2(int numberPoint) {
		centerPoint = new Point(getWidth() / 2, getHeight() / 2);
		init();
		R = (centerPoint.x > centerPoint.y) ? centerPoint.y : centerPoint.x;
		R = R * 3 / 4;
		int b = R / 2;
		for (int i = 1; i <= 9; i++) {
			int x = centerPoint.x - 5 * b + i * b;
			int y = centerPoint.y;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 7; i++) {
			int x = centerPoint.x - 4 * b + i * b;
			int y = centerPoint.y + b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 7; i++) {
			int x = centerPoint.x - 4 * b + i * b;
			int y = centerPoint.y - b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 5; i++) {
			int x = centerPoint.x - 3 * b + i * b;
			int y = centerPoint.y + 2 * b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 5; i++) {
			int x = centerPoint.x - 3 * b + i * b;
			int y = centerPoint.y - 2 * b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
	}

	// ve trai tim
	public void createGraph1(int numberPoint) {
		centerPoint = new Point(getWidth() / 2, getHeight() / 2);
		init();
		R = (centerPoint.x > centerPoint.y) ? centerPoint.y : centerPoint.x;
		R = R * 3 / 4;
		int b = R * 9 / 20;
		centerPoint.setLocation(centerPoint.x, centerPoint.y - b / 2);
		for (int i = 1; i <= 7; i++) {
			int x = centerPoint.x - 4 * b + i * b;
			int y = centerPoint.y;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 5; i++) {
			int x = centerPoint.x - 3 * b + i * b;
			int y = centerPoint.y + b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 3; i++) {
			int x = centerPoint.x - 2 * b + i * b;
			int y = centerPoint.y + b * 2;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		int x1 = centerPoint.x;
		int y1 = centerPoint.y + b * 3;
		data.getArrPoints().add(new Points(new Ellipse2D.Float(x1, y1, d, d)));
		for (int i = 1; i <= 7; i++) {
			int x = centerPoint.x - 4 * b + i * b;
			int y = centerPoint.y - b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 2; i++) {
			int x = centerPoint.x - 3 * b + i * b;
			int y = centerPoint.y - 2 * b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
		for (int i = 1; i <= 2; i++) {
			int x = centerPoint.x + i * b;
			int y = centerPoint.y - 2 * b;
			data.getArrPoints().add(new Points(new Ellipse2D.Float(x, y, d, d)));
		}
	}

	// open demo (random)
	public void readDemo(int demo) {
		drawResult = false;
		drawStep = false;
		drawTry = false;
		File file = new File("");
		String currentDirectory = file.getAbsolutePath() + "\\src\\input\\input" + demo + ".txt";
		FileInputStream fi;
		if (demo > 0)
			try {
				fi = new FileInputStream(currentDirectory);
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(fi);
				String input;
				while (scanner.hasNextLine()) {
					input = scanner.nextLine();
					String[] words = input.split("\\s");
					int numberPoint = Integer.parseInt(words[0]);
					int numberLine = Integer.parseInt(words[1]);
					createGraph(numberPoint);
					for (int i = 0; i < numberLine; i++) {
						int u, v, x;
						input = scanner.nextLine();
						words = input.split("\\s");
						u = Integer.parseInt(words[0]);
						v = Integer.parseInt(words[1]);
						x = Integer.parseInt(words[2]);
						data.getArrLines().add(new Lines(
								creatLine(data.getArrPoints().get(u).getPoint(),
										data.getArrPoints().get(v).getPoint()),
								u, v, x));
					}
				}
				repaint();
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "File not found", "Error", JOptionPane.ERROR_MESSAGE);
			}
	}

	// open file
	public void readFile(String path) {
		drawResult = false;
		drawStep = false;
		drawTry = false;
		FileInputStream fi;
		if (path.contains("testinput")) { // ve vien da
			try {
				fi = new FileInputStream(path);
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(fi);
				String input;
				while (scanner.hasNextLine()) {
					input = scanner.nextLine();
					String[] words = input.split("\\s");
					int numberPoint = Integer.parseInt(words[0]);
					int numberLine = Integer.parseInt(words[1]);
					createGraph2(numberPoint);
					for (int i = 0; i < numberLine; i++) {
						int u, v, x;
						input = scanner.nextLine();
						words = input.split("\\s");
						u = Integer.parseInt(words[0]);
						v = Integer.parseInt(words[1]);
						x = Integer.parseInt(words[2]);
						data.getArrLines().add(new Lines(
								creatLine(data.getArrPoints().get(u).getPoint(),
										data.getArrPoints().get(v).getPoint()),
								u, v, x));
					}
				}
				repaint();
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "File not found", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else if (path.contains("inputheart")) { // ve trai tim
			try {
				fi = new FileInputStream(path);
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(fi);
				String input;
				while (scanner.hasNextLine()) {
					input = scanner.nextLine();
					String[] words = input.split("\\s");
					int numberPoint = Integer.parseInt(words[0]);
					int numberLine = Integer.parseInt(words[1]);
					createGraph1(numberPoint);
					for (int i = 0; i < numberLine; i++) {
						int u, v, x;
						input = scanner.nextLine();
						words = input.split("\\s");
						u = Integer.parseInt(words[0]);
						v = Integer.parseInt(words[1]);
						x = Integer.parseInt(words[2]);
						data.getArrLines().add(new Lines(
								creatLine(data.getArrPoints().get(u).getPoint(),
										data.getArrPoints().get(v).getPoint()),
								u, v, x));
					}
				}
				repaint();
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "File not found", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			try {
				fi = new FileInputStream(path);
				@SuppressWarnings("resource")
				Scanner scanner = new Scanner(fi);
				String input;
				while (scanner.hasNextLine()) {
					input = scanner.nextLine();
					String[] words = input.split("\\s");
					int numberPoint = Integer.parseInt(words[0]);
					int numberLine = Integer.parseInt(words[1]);
					createGraph(numberPoint);
					for (int i = 0; i < numberLine; i++) {
						int u, v, x;
						input = scanner.nextLine();
						words = input.split("\\s");
						u = Integer.parseInt(words[0]);
						v = Integer.parseInt(words[1]);
						x = Integer.parseInt(words[2]);
						data.getArrLines().add(new Lines(
								creatLine(data.getArrPoints().get(u).getPoint(),
										data.getArrPoints().get(v).getPoint()),
								u, v, x));
					}
				}
				repaint();
			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, "File not found", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public int getDrawWith() {
		return drawWith;
	}

	public void setDrawWith(int drawWith) {
		this.drawWith = drawWith;
	}

	public int getDrawHeight() {
		return drawHeight;
	}

	public void setDrawHeight(int drawHeight) {
		this.drawHeight = drawHeight;
	}

	protected boolean isRightClick = false;
	protected Point pointRight;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public boolean isResetGraph() {
		return resetGraph;
	}

	public void setResetGraph(boolean resetGraph) {
		this.resetGraph = resetGraph;
	}

	public boolean isReDraw() {
		return reDraw;
	}

	public void setReDraw(boolean reDraw) {
		this.reDraw = reDraw;
	}

	public void setIndexBeginPoint(int indexBeginPoint) {
		this.indexBeginPoint = indexBeginPoint;
	}

	public int getIndexBeginPoint() {
		return indexBeginPoint;
	}

	public void setIndexEndPoint(int indexEndPoint) {
		this.indexEndPoint = indexEndPoint;
	}

	public int getIndexEndPoint() {
		return indexEndPoint;
	}

	public boolean[] getCheckedPointMin() {
		return checkedPointMin;
	}

	public void setCheckedPointMin(boolean[] checkedPointMin) {
		this.checkedPointMin = checkedPointMin;
	}

	public boolean isDrawStep() {
		return drawStep;
	}

	public void setDrawStep(boolean drawStep) {
		this.drawStep = drawStep;
	}

	public ArrayList<Integer> getArrPointResultStep() {
		return arrPointResultStep;
	}

	public void setArrPointResultStep(ArrayList<Integer> arrPointResultStep) {
		this.arrPointResultStep = arrPointResultStep;
	}

	public int[] getP() {
		return p;
	}

	public void setP(int[] p) {
		this.p = p;
	}

	public int[][] getA() {
		return a;
	}

	public void setA(int[][] a) {
		this.a = a;
	}

	public int getInfinity() {
		return infinity;
	}

	public void setInfinity(int infinity) {
		this.infinity = infinity;
	}

	public int[] getLen() {
		return len;
	}

	public void setLen(int[] len) {
		this.len = len;
	}

	public boolean isDrawResult() {
		return drawResult;
	}

	public void setDrawResult(boolean drawResult) {
		this.drawResult = drawResult;
	}

	public boolean isTypeMap() {
		return typeMap;
	}

	public void setTypeMap(boolean typeMap) {
		this.typeMap = typeMap;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public boolean isDrawTry() {
		return drawTry;
	}

	public void setDrawTry(boolean drawTry) {
		this.drawTry = drawTry;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int[] getDad() {
		return dad;
	}

	public void setDad(int[] dad) {
		this.dad = dad;
	}

	public ArrayList<Integer> getTrace() {
		return trace;
	}

	public void setTrace(ArrayList<Integer> trace) {
		this.trace = trace;
	}
}
