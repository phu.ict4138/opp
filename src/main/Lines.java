package main;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Line2D;
import java.io.Serializable;

public class Lines implements Serializable {

	private Line2D.Double line = new Line2D.Double();
	private int indexPointA, indexPointB;
	private int cost;
	private int r = Config.R;
	private int barb = Config.barb;
	private double phi = Config.phi;
	private Font font = Config.font;

	public Lines(Line2D.Double line, int indexPointA, int indexPointB, int cost) {
		this.cost = cost;
		this.indexPointA = indexPointA;
		this.indexPointB = indexPointB;
		this.line = line;
	}

	private void drawArrow(Graphics2D g, double theta, double x0, double y0,
			Color colorLine, int size) {
		g.setFont(font);
		double x = x0 - barb * Math.cos(theta + phi);
		double y = y0 - barb * Math.sin(theta + phi);
		g.setStroke(new BasicStroke((size + 1)));
		g.draw(new Line2D.Double(x0, y0, x, y));
		x = x0 - barb * Math.cos(theta - phi);
		y = y0 - barb * Math.sin(theta - phi);
		g.draw(new Line2D.Double(x0, y0, x, y));
	}

	public void drawLine(Graphics2D g, Point p1, Point p2, Color colorCost,
			Color colorLine, int size, boolean type) {
		String c = "";
		if (cost < 0) {
			c = "";
		} else
			c = String.valueOf(cost);
		g.setColor(colorLine);
		g.setFont(font);
		g.setStroke(new BasicStroke(size + 1));
		double theta = Math.atan2(p2.y - p1.y, p2.x - p1.x);
		g.draw(line);
		if (type && cost >= 0) {
			double x = p2.x - (r + 2) * Math.cos(theta);
			double y = p2.y - (r + 2) * Math.sin(theta);
			drawArrow(g, theta, x, y, colorLine, (size));
		}
		g.setColor(colorCost);
		g.drawString(c, (int) ((p1.x + p2.x) / 2), (int) (p1.y + p2.y) / 2);
	}

	public boolean containerPoint(Point p) {
		Polygon poly = createPolygon(line);
		for (int i = 0; i < poly.npoints; i++) {
			double temp = (p.x - poly.xpoints[i])
					* (poly.ypoints[(i + 1) % poly.npoints] - poly.ypoints[i])
					- (p.y - poly.ypoints[i])
							* (poly.xpoints[(i + 1) % poly.npoints] - poly.xpoints[i]);
			if (temp < 0)
				return false;
		}
		return true;
	}

	private Polygon createPolygon(Line2D line) {
		int barb = 5;
		double phi = Math.PI / 2;
		double theta = Math.atan2(line.getY2() - line.getY1(), line.getX2()
				- line.getX1());
		int x[] = new int[4];
		int y[] = new int[4];
		x[0] = (int) (line.getX1() - barb * Math.cos(theta + phi));
		y[0] = (int) (line.getY1() - barb * Math.sin(theta + phi));
		x[1] = (int) (line.getX1() - barb * Math.cos(theta - phi));
		y[1] = (int) (line.getY1() - barb * Math.sin(theta - phi));

		x[2] = (int) (line.getX2() - barb * Math.cos(theta - phi));
		y[2] = (int) (line.getY2() - barb * Math.sin(theta - phi));
		x[3] = (int) (line.getX2() - barb * Math.cos(theta + phi));
		y[3] = (int) (line.getY2() - barb * Math.sin(theta + phi));
		Polygon poly = new Polygon(x, y, 4);
		return poly;
	}

	public Line2D.Double getLine() {
		return this.line;
	}

	public void setLine(Line2D.Double line) {
		this.line = line;
	}

	public int getIndexPointA() {
		return this.indexPointA;
	}

	public void setIndexPointA(int indexPointA) {
		this.indexPointA = indexPointA;
	}

	public int getIndexPointB() {
		return this.indexPointB;
	}

	public void setIndexPointB(int indexPointB) {
		this.indexPointB = indexPointB;
	}

	public int getCost() {
		return this.cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
