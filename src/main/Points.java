package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.io.Serializable;

public class Points implements Serializable {
	private Point point = new Point();
	private Ellipse2D.Float ellipse = new Ellipse2D.Float();
	private int r = Config.R;
	private Font font = Config.font;

	public void drawIndex(Graphics2D g, int index, Color colorIndex) {
		g.setColor(colorIndex);
		g.setFont(font);
		int stringLen = (int) g.getFontMetrics()
				.getStringBounds(String.valueOf(index), g).getWidth();
		int stringHeight = (int) g.getFontMetrics()
				.getStringBounds(String.valueOf(index), g).getHeight();
		int startX = -stringLen / 2;
		int startY = stringHeight / 2;
		g.drawString(
				String.valueOf(index),
				startX + (int) point.x, (int) point.y + startY - 5);
	}

	public void drawCost(Graphics2D g, String cost, Color colorCostResult) {
		g.setColor(colorCostResult);
		g.setFont(font);
	}

	public void drawPoint(Graphics2D g, int index, Color colorPoint) {
		g.setColor(colorPoint);
		g.fill(ellipse);
		Graphics g2 = g;
		g2.setColor(Color.black);
		g2.drawOval(point.x - r, point.y - r, 2 * r, 2 * r);
	}

	public void drawIndexCost(Graphics2D g, int index, String cost,
			Color colorIndex, Color colorCostResult) {
		g.setFont(font);
		drawIndex(g, index, colorIndex);
		drawCost(g, cost, colorCostResult);
	}

	public void drawResult(Graphics2D g, int index, Color colorPoint,
			Color colorIndex, String cost, Color colorCostResult) {
		g.setFont(font);
		drawPoint(g, index, colorPoint);
		drawIndex(g, index, colorIndex);
		drawCost(g, cost, colorCostResult);
	}

	public void draw(Graphics2D g, int index, Color colorPoint, Color colorIndex) {
		Graphics2D g2 = g;
		g2.setColor(Color.black);
		g2.fillOval((point.x - r - 3), (point.y - r - 3), 2 * r + 6, 2 * r + 6);
		drawPoint(g, index, colorPoint);
		drawIndex(g, index, colorIndex);
	}

	public void setEllipse(Ellipse2D.Float ellipse) {
		this.ellipse = ellipse;
		this.point.x = (int) (ellipse.x + r);
		this.point.y = (int) (ellipse.y + r);
	}

	public Ellipse2D.Float getEllipse() {
		return this.ellipse;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public Point getPoint() {
		return this.point;
	}

	public Points(Ellipse2D.Float ellipse) {
		super();
		setEllipse(ellipse);
	}
}
